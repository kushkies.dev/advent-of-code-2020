use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
struct P(i32, i32);

#[derive(Debug)]
struct T(i32, i32, i32);

fn part1() {
  let numbers = read_numbers("data/input/day1");

  let result = numbers.clone()
                       .into_iter()
                       .find_map(|i| {
                         let j = &rem2020(&i);
                         match numbers.contains(j) {
                           true => Some((i, *j)),
                           false => None
                         }
                       })
                       .unwrap();

  println!("{:?}", result);
  println!("{}", result.0 + result.1);
  println!("{}", result.0 * result.1);
}

fn part2() {
  let numbers = read_numbers("data/input/day1");

  let result = numbers.clone()
                       .into_iter()
                       .find_map(|a| {
                         let b = numbers.clone()
                                        .into_iter()
                                        .find(|b| numbers.contains(&rem2020(&(a + b))))?;
                         Some((a, b))
                       })
                       .unwrap();
  let third = rem2020(&(result.0 + result.1));

  println!("{:?}", (result.0, result.1, third));
  println!("{}", result.0 + result.1 + third);
  println!("{}", result.0 * result.1 * third);
}

fn read_numbers(path: &str) -> Vec<i32> {
  let file = File::open(path).unwrap();

  return BufReader::new(file)
      .lines()
      .map(|l| l.unwrap().parse::<i32>().unwrap())
      .collect();
}

fn rem2020(i: &i32) -> i32 {
  2020 - i
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test1() {
    part1();
  }

  #[test]
  fn test2() {
    part2();
  }
}
