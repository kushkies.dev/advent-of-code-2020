use std::fs::File;
use std::io::{BufRead, BufReader};
use regex::{Captures, Regex};

fn part1() {
  let count = valid_password_count("data/input/day2",
                                   |captures| {
                               let count = &captures[4].matches(&captures[3]).count();
                               count >= &captures[1].parse().unwrap() && count <= &captures[2].parse().unwrap()
                             });

  println!("{}", count);
}

fn part2() {
  let count = valid_password_count("data/input/day2",
                                   |captures| {
                               let index1 = *&captures[1].parse::<usize>().unwrap() - 1;
                               let index2 = *&captures[2].parse::<usize>().unwrap() - 1;
                               let char = *&captures[3].chars().nth(0).unwrap();
                               let line = &captures[4];
                               (line.chars().nth(index1).unwrap() == char) ^
                                       (line.chars().nth(index2).unwrap() == char)
                             });

  println!("{}", count);
}

fn valid_password_count<F: Fn(Captures) -> bool>(path: &str, f: F) -> usize {
  let file = File::open(path).unwrap();
  let regex = Regex::new(r"(\d+)-(\d+) (\w): (\w+)").unwrap();
  return BufReader::new(file)
      .lines()
      .map(|l|
          regex.captures(l.unwrap().as_str())
               .map(&f)
      )
      .filter(|o| o.unwrap())
      .count();
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test1() {
    part1();
  }

  #[test]
  fn test2() {
    part2();
  }
}
