use std::fs::File;
use std::io::{BufRead, BufReader};
use std::io::Error;

fn part1() {
  let count = number_of_trees_hit("data/input/day3",
                                  |(i, l)| (i * 3, Some(l.unwrap()))
  );
  println!("{}", count);
}

fn part2() {
  let slopes: Vec<fn((usize, Result<String, Error>)) -> (usize, Option<String>)> = vec!(
    |(i, l)| (i, validate_row(i, 1, l)),
    |(i, l)| (i * 3, validate_row(i, 1, l)),
    |(i, l)| (i * 5, validate_row(i, 1, l)),
    |(i, l)| (i * 7, validate_row(i, 1, l)),
    |(i, l)| (div_by_2(i), validate_row(i, 2, l)),
  );

  let multiplied = slopes
      .into_iter()
      .map(|slope| number_of_trees_hit("data/input/day3", slope))
      .fold(1, |i, j| i * j);

  println!("{}", multiplied);
}

fn div_by_2(i: usize) -> usize {
  if i > 0 {
    i / 2
  } else {
    i
  }
}

fn number_of_trees_hit(
  path: &str,
  slope: fn((usize, Result<String, Error>)) -> (usize, Option<String>)
) -> usize {
  let file = File::open(path).unwrap();

  return BufReader::new(file)
      .lines()
      .enumerate()
      .map(slope)
      .map(|(mut idx, l)| {
        match l {
          Some(line) => {
            while idx >= line.len() {
              idx -= line.len()
            }
            line.chars().nth(idx).unwrap()
          },
          None => ' '
        }
      })
      .filter(|c| *c == '#')
      .count();
}

fn validate_row(i: usize, rem: usize, l: Result<String, Error>) -> Option<String> {
  if i % rem == 0 {
    Some(l.unwrap())
  } else {
    None
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test1() {
    part1();
  }

  #[test]
  fn test2() {
    part2();
  }
}
